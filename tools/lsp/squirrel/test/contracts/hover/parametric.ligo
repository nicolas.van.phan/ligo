function k<a, b>(const a : a; const _ : b) : a is (a : a)

function main(const p : unit; const _ : unit) : list(operation) * unit is
  (nil, p)
