let id = (type a, a : a) => (a : a);

let main = (_ : (unit, unit)) : (list(operation), unit) =>
  ([], id(()));
