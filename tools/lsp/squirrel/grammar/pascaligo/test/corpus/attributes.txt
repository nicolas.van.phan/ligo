=========
sum
=========

type foo is [@boom] | True | False

----

(source_file
  (type_decl
    (TypeName)
    (sum_type
      (Attr)
      (variant (ConstrName))
      (variant (ConstrName))
    )
  )
)


=========
constr
=========

type foo is [@boom] True | False

----

(source_file
  (type_decl
    (TypeName)
    (sum_type
      (variant (Attr) (ConstrName))
      (variant (ConstrName))
    )
  )
)


=========
constr with bar
=========

type foo is | [@boom] True | False

----

(source_file
  (type_decl
    (TypeName)
    (sum_type
      (variant (Attr) (ConstrName))
      (variant (ConstrName))
    )
  )
)


=========
record
=========

type person is [@layout:comb] record [
  name: string;
]

----

(source_file
  (type_decl
    (TypeName)
    (record_type
      (Attr)
      (field_decl (FieldName) (TypeName))
    )
  )
)


=========
field twice
=========

type person is record [
  [@foo][@bar] name: string;
]

----

(source_file
  (type_decl
    (TypeName)
    (record_type
      (field_decl (Attr) (Attr) (FieldName) (TypeName))
    )
  )
)


=========
function
=========

[@inline] function foo (const a : int) : int is
  block {
    const test : a = 1;
  } with test;

---

(source_file
  (fun_decl
    (Attr)
    (NameDecl)
    (param_decl (var_pattern (NameDecl)) (TypeName))
    (TypeName)
    (block_with
      (block
        (const_decl (var_pattern (NameDecl)) (TypeName) (Int))
      )
      (Name)
    )
  )
)


=========
const
=========

[@annot] const x : int = 1;

---

(source_file
  (const_decl
    (Attr)
    (var_pattern (NameDecl))
    (TypeName)
    (Int)
  )
)


=========
const twice
=========

[@annot1][@annot2] const x : int = 1;

---

(source_file
  (const_decl
    (Attr) (Attr)
    (var_pattern (NameDecl))
    (TypeName)
    (Int)
  )
)


=========
local const
=========

function foo (const a : int) : int is
  block {
    [@inline] const test : int = 1;
  } with test;

---

(source_file
  (fun_decl
    (NameDecl)
    (param_decl (var_pattern (NameDecl)) (TypeName))
    (TypeName)
    (block_with
      (block
        (const_decl (Attr) (var_pattern (NameDecl)) (TypeName) (Int))
      )
      (Name)
    )
  )
)
